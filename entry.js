const fetch = require("node-fetch")

module.exports = class {
    constructor(baseurl, pass) {
        this.base = baseurl
        this.pass = pass
        this.active = true
    }

    setActiveState(state) {
        this.active = !!state
    }

    push(event, content) {
        return new Promise((res, rej) => {
            fetch(`${this.base}/event/push`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    eventname: event,
                    content,
                    pass: this.pass
                })
            }).then(rs => rs.json().then(r => {
                if (r.success === true) (res())
                else rej(r)
            }))
        })
    }

    getAll() {
        return new Promise((res, rej) => {
            fetch(`${this.base}/event/all`, {
                headers: {
                    Authorization: this.pass
                }
            }).then(rs => rs.json().then(r => {
                if (rs.ok) (res(r))
                else rej(r)
            }))
        })
    }
}
