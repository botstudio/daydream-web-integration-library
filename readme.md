# ![Daydream Web Integration Library](logo-full.png)  
  
### API  
  
```js
const DDWL = require("dd-weblib")  
const events = new DDWL(baseurl, password)

// Push an event
events.push("COMMAND", "dd-help").then(() => {
    console.log("Pushed an event.")
}).catch(e => {
    console.error(e)
})

// Get all events
events.getAll().then(events => {
    console.log("Events:", events)
}).catch(e => {
    console.error(e)
})
```